<?php include 'component/header.php';?>

<!-- Slider Section
================================================== -->
<main class="root">
    <div class="dsn-grid-color">
        <div class="dsn-grid-root dsn-grid-slider">

            <div class="dsn-progress"></div>

            <div class="dsn-progress-circle">
                <svg width="100%" height="100%" stroke="#fff" viewBox="0 0 100 100" preserveAspectRatio="xMinYMin meet"
                     fill="#1b1b1b">
                    <path class="dsn-progress-path" d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" style="transition: stroke-dashoffset 300ms linear 0s; stroke-dasharray: 307.919, 307.919; stroke-dashoffset: 309;"></path>
                </svg>
            </div>

            <!-- Slider Content -->
            <div class="dsn-grid-content">
                <div class="dsn-grid-wrapper">

                    <!-- Slider Prev -->
                    <div class="dsn-grid-prev">
                        <div class="img cover-bg" data-id="3" data-image-src="assets/img/bangabandhu/7thmarch.jpg"></div>
                        <div class="img cover-bg" data-id="5" data-image-src="assets/img/ilias/slider-2.jpg"></div>
                        <div class="img cover-bg" data-id="1"
                             data-image-src="assets/img/bangabandhu/10thJanuary.jpg" data-overlay="2"></div>
                        <div class="img cover-bg" data-id="6" data-image-src="assets/img/ilias/slider-3.jpg"></div>
                        <div class="img cover-bg" data-id="2" data-image-src="assets/img/bangabandhu/15august.jpg"></div>
                        <div class="img cover-bg" data-id="4" data-image-src="assets/img/ilias/slider-1.jpg"></div>


                    </div>
                    <!-- End Slider Prev -->

                    <!--  Slider Info -->
                    <div class="dsn-grid-info">
                        <div class="dsn-grid-info-wrapper dsn-slider-active" data-id="1" data-url="project1.html">
                            <h6 class="slide-content__kicker">
                                <span>— Bangabandhu in </span>
                            </h6>
                            <div class="title">
                                <h2>7th March, A Dream to Freedom.</h2>
                            </div>




                            <a href="#" class="custom-btn">
                                <span class="custom-btn__label">View Full Image</span>

                                <span class="custom-btn__icon">

                                        <span class="custom-btn__icon-small">
                                            <!--?xml version="1.0" encoding="utf-8"?-->
                                            <!-- Generator: Adobe Illustrator 22.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve">
                                                <polygon points="33.7,95.8 27.8,90.5 63.9,50 27.8,9.5 33.7,4.2 74.6,50 "></polygon>
                                            </svg>

                                        </span>

                                        <span class="custom-btn__icon-circle">
                                            <!--?xml version="1.0" encoding="utf-8"?-->
                                            <!-- Generator: Adobe Illustrator 22.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve">
                                                <path class="bottomcircle" d="M18.2,18.2c17.6-17.6,46-17.6,63.6,0s17.6,46,0,63.6s-46,17.6-63.6,0"></path>
                                                <path class="topcircle" d="M18.2,18.2c17.6-17.6,46-17.6,63.6,0s17.6,46,0,63.6s-46,17.6-63.6,0"></path>
                                            </svg>

                                        </span>


                                    </span>
                            </a>
                        </div>




                        <div class="dsn-grid-info-wrapper dsn-slider-active" data-id="5" data-url="project1.html">
                            <h6 class="slide-content__kicker">
                                <span>— Ilias Ahmed </span>
                            </h6>
                            <div class="title">
                                <h2>Ilias Ahmed</h2>
                            </div>




                            <a href="#" class="custom-btn">
                                <span class="custom-btn__label">View Full Image</span>

                                <span class="custom-btn__icon">

                                        <span class="custom-btn__icon-small">
                                            <!--?xml version="1.0" encoding="utf-8"?-->
                                            <!-- Generator: Adobe Illustrator 22.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve">
                                                <polygon points="33.7,95.8 27.8,90.5 63.9,50 27.8,9.5 33.7,4.2 74.6,50 "></polygon>
                                            </svg>

                                        </span>

                                        <span class="custom-btn__icon-circle">
                                            <!--?xml version="1.0" encoding="utf-8"?-->
                                            <!-- Generator: Adobe Illustrator 22.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve">
                                                <path class="bottomcircle" d="M18.2,18.2c17.6-17.6,46-17.6,63.6,0s17.6,46,0,63.6s-46,17.6-63.6,0"></path>
                                                <path class="topcircle" d="M18.2,18.2c17.6-17.6,46-17.6,63.6,0s17.6,46,0,63.6s-46,17.6-63.6,0"></path>
                                            </svg>

                                        </span>


                                    </span>
                            </a>
                        </div>
                        <div class="dsn-grid-info-wrapper" data-id="2" data-url="project2.html">
                            <h6 class="slide-content__kicker">
                                <span>— Welcome Back Leader</span>
                            </h6>
                            <div class="title">
                                <h2>  10th January Home Coming Day.
                                </h2>
                            </div>



                            <a href="#" class="custom-btn">
                                <span class="custom-btn__label">View  Full Image</span>

                                <span class="custom-btn__icon">
                                        <span class="custom-btn__icon-small">
                                            <!--?xml version="1.0" encoding="utf-8"?-->
                                            <!-- Generator: Adobe Illustrator 22.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve">
                                                <polygon points="33.7,95.8 27.8,90.5 63.9,50 27.8,9.5 33.7,4.2 74.6,50 "></polygon>
                                            </svg>

                                        </span>
                                        <span class="custom-btn__icon-circle">
                                            <!--?xml version="1.0" encoding="utf-8"?-->
                                            <!-- Generator: Adobe Illustrator 22.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve">
                                                <path class="bottomcircle" d="M18.2,18.2c17.6-17.6,46-17.6,63.6,0s17.6,46,0,63.6s-46,17.6-63.6,0"></path>
                                                <path class="topcircle" d="M18.2,18.2c17.6-17.6,46-17.6,63.6,0s17.6,46,0,63.6s-46,17.6-63.6,0"></path>
                                            </svg>

                                        </span>
                                    </span>
                            </a>
                        </div>


                        <div class="dsn-grid-info-wrapper" data-id="6" data-url="project2.html">
                            <h6 class="slide-content__kicker">
                                <span>— Ilias Ahmed</span>
                            </h6>
                            <div class="title">
                                <h2>  Ilias Ahmed
                                </h2>
                            </div>



                            <a href="#" class="custom-btn">
                                <span class="custom-btn__label">View  Full Image</span>

                                <span class="custom-btn__icon">
                                        <span class="custom-btn__icon-small">
                                            <!--?xml version="1.0" encoding="utf-8"?-->
                                            <!-- Generator: Adobe Illustrator 22.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve">
                                                <polygon points="33.7,95.8 27.8,90.5 63.9,50 27.8,9.5 33.7,4.2 74.6,50 "></polygon>
                                            </svg>

                                        </span>
                                        <span class="custom-btn__icon-circle">
                                            <!--?xml version="1.0" encoding="utf-8"?-->
                                            <!-- Generator: Adobe Illustrator 22.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve">
                                                <path class="bottomcircle" d="M18.2,18.2c17.6-17.6,46-17.6,63.6,0s17.6,46,0,63.6s-46,17.6-63.6,0"></path>
                                                <path class="topcircle" d="M18.2,18.2c17.6-17.6,46-17.6,63.6,0s17.6,46,0,63.6s-46,17.6-63.6,0"></path>
                                            </svg>

                                        </span>
                                    </span>
                            </a>
                        </div>

                        <div class="dsn-grid-info-wrapper" data-id="3" data-url="project3.html">
                            <h6 class="slide-content__kicker">
                                    <span>— The National Mourning Day </span>
                            </h6>
                            <div class="title">
                                <h2>Bangabandhu Pass Away (Inna Lillahi wa inna ilayhi raji'un)
                                </h2>
                            </div>



                            <a href="#" class="custom-btn">
                                <span class="custom-btn__label">View  Full Image</span>

                                <span class="custom-btn__icon">

                                        <span class="custom-btn__icon-small">
                                            <!--?xml version="1.0" encoding="utf-8"?-->
                                            <!-- Generator: Adobe Illustrator 22.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve">
                                                <polygon points="33.7,95.8 27.8,90.5 63.9,50 27.8,9.5 33.7,4.2 74.6,50 "></polygon>
                                            </svg>

                                        </span>

                                        <span class="custom-btn__icon-circle">
                                            <!--?xml version="1.0" encoding="utf-8"?-->
                                            <!-- Generator: Adobe Illustrator 22.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve">
                                                <path class="bottomcircle" d="M18.2,18.2c17.6-17.6,46-17.6,63.6,0s17.6,46,0,63.6s-46,17.6-63.6,0"></path>
                                                <path class="topcircle" d="M18.2,18.2c17.6-17.6,46-17.6,63.6,0s17.6,46,0,63.6s-46,17.6-63.6,0"></path>
                                            </svg>

                                        </span>


                                    </span>
                            </a>
                        </div>

                        <div class="dsn-grid-info-wrapper" data-id="4" data-url="project3.html">
                            <h6 class="slide-content__kicker">
                                    <span>— Ilias Ahmed </span>
                            </h6>
                            <div class="title">
                                <h2>Ilias Ahmed
                                </h2>
                            </div>



                            <a href="#" class="custom-btn">
                                <span class="custom-btn__label">View  Full Image</span>

                                <span class="custom-btn__icon">

                                        <span class="custom-btn__icon-small">
                                            <!--?xml version="1.0" encoding="utf-8"?-->
                                            <!-- Generator: Adobe Illustrator 22.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve">
                                                <polygon points="33.7,95.8 27.8,90.5 63.9,50 27.8,9.5 33.7,4.2 74.6,50 "></polygon>
                                            </svg>

                                        </span>

                                        <span class="custom-btn__icon-circle">
                                            <!--?xml version="1.0" encoding="utf-8"?-->
                                            <!-- Generator: Adobe Illustrator 22.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve">
                                                <path class="bottomcircle" d="M18.2,18.2c17.6-17.6,46-17.6,63.6,0s17.6,46,0,63.6s-46,17.6-63.6,0"></path>
                                                <path class="topcircle" d="M18.2,18.2c17.6-17.6,46-17.6,63.6,0s17.6,46,0,63.6s-46,17.6-63.6,0"></path>
                                            </svg>

                                        </span>


                                    </span>
                            </a>
                        </div>

                    </div>
                    <!-- End Slider Info -->

                    <!-- Slider veiw Box -->
                    <div class="dsn-grid-current">
                        <div class="dsn-grid-hover-label"></div>
                        <div class="dsn-grid-hover-pic"></div>
                        <div class="dsn-grid-slider-effect">
                            <div class="img cover-bg" data-id="1" data-image-src="assets/img/bangabandhu/7thmarch.jpg"
                                 data-overlay="2"></div>
                            <div class="img cover-bg" data-id="5" data-image-src="assets/img/ilias/slider-2.jpg"></div>
                            <div class="img cover-bg" data-id="2" data-image-src="assets/img/bangabandhu/10thJanuary.jpg"></div>
                            <div class="img cover-bg" data-id="6" data-image-src="assets/img/ilias/slider-3.jpg"></div>
                            <div class="img cover-bg" data-id="3" data-image-src="assets/img/bangabandhu/15august.jpg"></div>
                            <div class="img cover-bg" data-id="4" data-image-src="assets/img/ilias/slider-1.jpg"></div>
                        </div>

                    </div>
                    <!-- End Slider veiw Box -->

                    <!-- Slider Next -->
                    <div class="dsn-grid-nav-box">
                        <div class="to_top to-next" data-dsn-grid="parallax" data-dsn-grid-move="20">
                            <a href="#"></a>
                            <div class="icon-circle"></div>
                            <div id="to-top">
                                <svg viewBox="0 0 40 40" class="js-hover-it js-hit-icon">
                                    <path class="path circle" d="M20,2A18,18,0,1,1,2,20,18,18,0,0,1,20,2"></path>
                                    <polyline class="path" points="25.4 22.55 20 17.15 14.6 22.55"></polyline>
                                </svg>
                            </div>
                        </div>

                        <div class="dsn-grid-num">
                            <a href="#">
                                <span>01</span>
                            </a>
                        </div>


                        <div class="to_bottom to-next" data-dsn-grid="parallax" data-dsn-grid-move="20">
                            <div class="icon-circle"></div>
                            <a href="#"></a>
                            <div id="to-bottom">
                                <svg viewBox="0 0 40 40" class="js-hover-it js-hit-icon">
                                    <path class="path circle" d="M20,2A18,18,0,1,1,2,20,18,18,0,0,1,20,2"></path>
                                    <polyline class="path" points="14.6 17.45 20 22.85 25.4 17.45"></polyline>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <!-- End Slider Next -->

                </div>
            </div>
            <!-- End Slider Content -->

        </div>
    </div>
</main>
<!-- End Slider Section
================================================== -->
<?php include 'component/homePageFooter.php';?>
