<!DOCTYPE html>
<html lang="en-US">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="discrption"
          content="Sheikh Mujibur Rahman (Bengali: শেখ মুজিবুর রহমান); (17 March 1920 – 15 August 1975), shortened as Sheikh Mujib or just Mujib, was a Bangladeshi politician and statesman.">
    <meta name="keyword"
          content="Sheikh Mujibur Rahman (Bengali: শেখ মুজিবুর রহমান); (17 March 1920 – 15 August 1975), shortened as Sheikh Mujib or just Mujib, was a Bangladeshi politician and statesman.">

    <!--  Title -->
    <title>Bangabandhu Sheikh Mujibur Rahman</title>
    <link href="assets/css/popup.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <!--
    This is a smiple project using javascript arrays, random number generator, string methods, and a jquery event handler.

    Background jpg from https://subtlepatterns.com/
    Fonts from Google Fonts
    -->

    <link href="https://fonts.googleapis.com/css?family=Cardo:400,400italic|Radley|Tangerine" rel="stylesheet">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-3">
            <h1>যেখানে দেখিবে ছাই, উড়াইয়া দেখিয়ো তাই
                পাইলেও পাইতে পারো অমূল্য রতন..</h1>
            <p>হ্যাঁ, প্রিয় আদর্শ, প্রিয় বঙ্গবন্ধু.. এবার অনলাইনেও হবে তোমার যথার্থ মূল্যায়ন.. যুব সমাজ তোমার মাঝেই
                খুঁজবে তার নিজ উন্নয়ন.. <br> প্রচারে এ, এস, এম, নূর-ই-আলম (নাভীম) <br> বঙ্গবন্ধুর কিছু প্রিয় বাণীসমূহঃ
            </p>
            <button class=button> বাণীসমূহ </button>
            <div class="quote"><span class="saying"></span><br><span class="author"></span></div>

        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        generate();

        function generate() {
            var quotes = ["এটাই হয়ত আমার শেষ বার্তা, আজ থেকে বাংলাদেশ স্বাধীন। আমি বাংলাদেশের মানুষকে আহ্বান জানাই, আপনারা যেখানেই থাকুন, আপনাদের সর্বস্ব দিয়ে দখলদার সেনাবাহিনীর বিরুদ্ধে শেষ পর্যন্ত প্রতিরোধ চালিয়ে যান। বাংলাদেশের মাটি থেকে সর্বশেষ পাকিস্তানি সৈন্যটিকে উত্খাত করা এবং চূড়ান্ত বিজয় অর্জনের আগ পর্যন্ত আপনাদের যুদ্ধ অব্যাহত থাকুক।", "ব্ল্যাকমার্কেটিং কারা করে? যাদের পেটের মধ্যে দুই কলম বিদ্যা রয়েছে তারাই ব্ল্যাকমার্কেটিং করে। স্মাগলিং কারা করে? যারা বেশি লেখাপড়া করেছে তারাই করে। হাইজাকিং কারা করে? যারা বেশি লেখাপড়া শিখছে তারাই করে। ইন্টারন্যাশনাল স্মাগলিং তারাই করে। বিদেশে টাকা রাখে তারাই। আমরা যারা শিক্ষিত, আমরা যারা বুদ্বিমান, ওষুধের মধ্যে ভেজাল দিয়ে বিষাক্ত করে মানুষকে খাওয়াই তারাই। নিশ্চয়ই গ্রামের লোক এসব পারে না, নিশ্চয়ই আমার কৃষক ভাইরা পারে না। নিশ্চয়ই আমার শ্রমিক ভাইরা পারে না।", "খবরের কাগজে পড়বার মতো কিছূ থাকে না। একঘেয়ে সংবাদ। প্রেসিডেন্ট আইয়ুব কী বললেন, কী করলেন, কোথায় গেলেন, কার সাথে দেখা করলেন, দেশের উন্নতি, অগ্রগতি, গুদাম ভরা খাদ্য, অভাব নাই, বিরাট বিরাট প্রজেক্ট গ্রহন করা হয়েছে, কাজ শুরু হয়েছে ইত্যাদি ইত্যাদি। কেহ কেহ মুরব্বিয়ানাচালে দেশপ্রেমিকের সার্টীফিকেটও দিয়ে থাকেন। দুনিয়ায় নাকি পাকিস্তানের সম্মান এতো বেড়ে গেছে যে আসমান প্রায় ধরে ফেলেছে। নানা বেহুদা প্রশংসা, তবুও তাই পড়তে হবে।", "এ প্রধানমন্ত্রীত্ব আমার কাছে কাঁটা বলে মনে হয়। আমি যদি বাংলার মানুষের মুখে হাসি ফোটাতে না পারি, আমি যদি দেখি বাংলার মানুষ দু:খী, আর যদি দেখি বাংলার মানুষ পেট ভরে খায় নাই, তাহলে আমি শান্তিতে মরতে পারবো না।"
            ];


            randomQuote = quotes[Math.floor(Math.random() * quotes.length)];
            quoteAuthor = randomQuote.split("@");
            $('.saying').text(quoteAuthor[0]);
            $('.author').text(quoteAuthor[1]);
        }


        $(".button").on("click", function () {
            generate();
        });

    });

</script>
</body>
</html>