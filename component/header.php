<!DOCTYPE html>
<html>

<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="discrption" content="Sheikh Mujibur Rahman (Bengali: শেখ মুজিবুর রহমান); (17 March 1920 – 15 August 1975), shortened as Sheikh Mujib or just Mujib, was a Bangladeshi politician and statesman.">
    <meta name="keyword" content="Sheikh Mujibur Rahman (Bengali: শেখ মুজিবুর রহমান); (17 March 1920 – 15 August 1975), shortened as Sheikh Mujib or just Mujib, was a Bangladeshi politician and statesman.">

    <!--  Title -->
    <title>Bangabandhu Sheikh Mujibur Rahman</title>

    <!-- Font Google -->
    <link href="assets/css/css35aa.css?family=Poppins:400,600,700,900" rel="stylesheet">
    <link href="assets/css/cssa872.css?family=Playfair+Display:400,700,900" rel="stylesheet">
    <link href="assets/css/css4146.css?family=Fjalla+One&amp;subset=latin-ext" rel="stylesheet">

    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="http://www.jacklmoore.com/colorbox/example1/colorbox.css" />

    <!-- custom styles (optional) -->
    <link href="assets/css/plugins.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/responsive.css" rel="stylesheet">
    <script>
        function openColorBox(){
            $.colorbox({iframe:true, width:"80%", height:"80%", href: "popup.php"});
        }

        function countDown(){
            seconds--
            $("#seconds").text(seconds);
            if (seconds === 0){
                openColorBox();
                clearInterval(i);
            }
        }

        var seconds = 1,
            i = setInterval(countDown, 1000);
    </script>

</head>

<body>


<!--  DSN Loader
================================================== -->
<div class="dsn-loader">
    <div class="dsn-up"></div>
    <div class="dsn-progress-page"></div>
    <div class="dsn-down"></div>
</div>
<!-- End DSN Loader
================================================== -->

<!-- Sticky Menu
================================================== -->
<div class="header-top nav-mobile">
    <div class="header-container">

        <div class="logo">
            <a href="#">
                <img src="assets/img/favicon.png" alt="">
            </a>
        </div>

        <div class="menu-icon" data-dsn-grid="parallax" data-dsn-grid-move="50">
            <div class="icon-m">
                <span class="menu-icon__line menu-icon__line-left"></span>
                <span class="menu-icon__line"></span>
                <span class="menu-icon__line menu-icon__line-right"></span>
            </div>
        </div>

        <div class="nav">
            <div class="inner">
                <div class="logo">
                    <a href="#">
                        <img src="assets/img/favicon.png" alt="">
                    </a>
                </div>
                <div class="nav__content">
                    <ul class="nav__list">
                        <li><a href="index.php">Home</a></li>

                        <li><a href="https://en.wikipedia.org/wiki/Sheikh_Mujibur_Rahman">About</a></li>
                    </ul>
                    <address>
                        Bangabandhu Sheikh Mujibur Rahma<br>
                        (17 March 1920 – 15 August 1975)
                    </address>
                </div>
            </div>

        </div>
    </div>

</div>
<div class="site-header">
    <div class="extend-container">
        <div class="inner-header">
            <div class="main-logo">
                <a href="index.php">
                    <img src="assets/img/favicon.png" alt="">
                </a>
            </div>
        </div>
        <nav class=" accent-menu main-navigation">
            <ul class="extend-container">

                <li><a href="index.php">Home</a></li>

                <li><a href="https://en.wikipedia.org/wiki/Sheikh_Mujibur_Rahman">About</a></li>

            </ul>
        </nav>
    </div>
</div>
<!-- End Sticky Menu
================================================== -->